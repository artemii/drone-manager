package com.artemii.musalasoft.service;

import com.artemii.musalasoft.exception.DroneNotFoundException;
import com.artemii.musalasoft.model.*;
import com.artemii.musalasoft.repository.DroneRepository;
import com.artemii.musalasoft.repository.MedicationRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.transaction.Transactional;
import java.util.Collections;
import java.util.List;

@ExtendWith({SpringExtension.class})
@SpringBootTest
class DroneServiceImplTest {

    @Autowired
    private DroneService droneService;

    @Autowired
    private MedicationService medicationService;

    @Autowired
    private DroneRepository droneRepository;
    @Autowired
    private MedicationRepository medicationRepository;

    @BeforeEach
    void setUp() {
        droneRepository.deleteAll();
        medicationRepository.deleteAll();
    }

    @Test
    void register() {
        String serialNumber = "JKHUq67hhIEgY";
        Drone drone = buildTestDrone(serialNumber);
        Drone registeredDrone = droneService.register(drone);
        Assertions.assertNotNull(registeredDrone.getId());

        Assertions.assertEquals(serialNumber, droneService.getBySerialNumber(serialNumber).getSerialNumber());
    }

    private Drone buildTestDrone(String serialNumber) {
        Drone drone = new Drone();
        drone.setSerialNumber(serialNumber);
        drone.setModel(DroneModel.Cruiserweight);
        drone.setWeightLimit(475);
        drone.setBatteryCapacity(80);
        drone.setState(DroneState.IDLE);
        return drone;
    }

    @Test
    void getBySerialNumber() {
        String serialNumber = "JKHUq67hhIEgY";
        droneService.register(buildTestDrone(serialNumber));

        Assertions.assertEquals(serialNumber, droneService.getBySerialNumber(serialNumber).getSerialNumber());
    }

    @Test
    void getByNotExistSerialNumber() {
        DroneNotFoundException thrown = Assertions.assertThrows(
                DroneNotFoundException.class,
                () ->  droneService.getBySerialNumber("12ffdsf")
        );
        Assertions.assertNotNull(thrown);
    }

    @Test
    void addMedicationItems() {
        String serialNumber = "JKHUq67hhIEgY";
        droneService.register(buildTestDrone(serialNumber));

        String medicationCode = "N02B";
        Medication medication = buildTestMedication(medicationCode);
        medicationService.register(medication);

        MedicationPayload medicationPayload = new MedicationPayload();
        medicationPayload.setMedicationCode(medicationCode);
        medicationPayload.setCount(2);
        Drone loadedDrone = droneService.addMedicationItems(serialNumber, Collections.singletonList(medicationPayload));
        Assertions.assertEquals(1, loadedDrone.getMedicationItems().size());
        Assertions.assertEquals(medicationCode, loadedDrone.getMedicationItems().get(0).getMedication().getCode());
    }

    private Medication buildTestMedication(String code) {
        Medication medication = new Medication();
        medication.setCode(code);
        medication.setImage("analgin.png");
        medication.setName("analgin");
        medication.setWeight(200);
        return medication;
    }

    @Test
    void getBatteryLevel() {
        String serialNumber = "JKHUq67hhIEgY";
        droneService.register(buildTestDrone(serialNumber));

        Assertions.assertEquals(80, droneService.getBySerialNumber(serialNumber).getBatteryCapacity());
    }

    @Test
    void getAvailableDrones() {
        String serialNumber1 = "JKHUq67hhIEgY";
        Drone drone1 = buildTestDrone(serialNumber1);
        droneService.register(drone1);

        String serialNumber2 = "JKHUq67hhIEgY2";
        Drone drone2 = buildTestDrone(serialNumber2);
        drone2.setState(DroneState.LOADING);
        droneService.register(drone2);

        List<Drone> availableDrones = droneService.getAvailableDrones();
        Assertions.assertEquals(1, availableDrones.size());
        Assertions.assertEquals(serialNumber1, availableDrones.get(0).getSerialNumber());


    }
}