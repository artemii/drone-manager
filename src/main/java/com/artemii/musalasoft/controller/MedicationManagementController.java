package com.artemii.musalasoft.controller;

import com.artemii.musalasoft.model.ApiResponse;
import com.artemii.musalasoft.model.Medication;
import com.artemii.musalasoft.service.MedicationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/medication/")
@Validated
public class MedicationManagementController {

    private MedicationService medicationService;

    @Autowired
    public MedicationManagementController(MedicationService medicationService) {
        this.medicationService = medicationService;
    }

    @PostMapping("register")
    public ResponseEntity<ApiResponse> registerMedication(@Valid @RequestBody Medication medication) {
        Medication registeredMedication = medicationService.register(medication);
        return new ResponseEntity<>(new ApiResponse("medication was registered successfully", registeredMedication),
                HttpStatus.CREATED);
    }

}
