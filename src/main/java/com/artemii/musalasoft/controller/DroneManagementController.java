package com.artemii.musalasoft.controller;

import com.artemii.musalasoft.model.*;
import com.artemii.musalasoft.service.DroneService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/api/drone/")
@Validated
public class DroneManagementController {

    private DroneService droneService;

    @Autowired
    public DroneManagementController(DroneService droneService) {
        this.droneService = droneService;
    }

    @PostMapping("register")
    public ResponseEntity<ApiResponse> registerDrone(@Valid @RequestBody Drone drone) {
        Drone registeredDrone = droneService.register(drone);
        return new ResponseEntity<>(new ApiResponse("drone was registered successfully", registeredDrone),
                HttpStatus.CREATED);
    }

    @PostMapping("{serialNumber}/medication/load")
    public ResponseEntity<ApiResponse> medicationLoading(@Valid @RequestBody List<MedicationPayload> medicationPayload,
                                                         @PathVariable String serialNumber) {
        if (!checkDuplicates(medicationPayload)) {
            return new ResponseEntity<>(new ApiResponse(
                    "there are medication duplicates. You have to use 'count' parameter to specify amount of specific medication",
                    medicationPayload
            ), HttpStatus.BAD_REQUEST);
        }
        Drone drone = droneService.getBySerialNumber(serialNumber);
        if (drone.getState() != DroneState.IDLE) {
            return new ResponseEntity<>(new ApiResponse(
                    "the drone is not available. Drone state: " + drone.getState(),
                    drone
            ), HttpStatus.BAD_REQUEST);
        }
        if (drone.getBatteryCapacity() < 25) {
            return new ResponseEntity<>(new ApiResponse(
                    "Drone is not available! Battery Capacity: " + drone.getBatteryCapacity() + "%",
                    drone
            ), HttpStatus.BAD_REQUEST);
        }

        Drone loadedDrone = droneService.addMedicationItems(serialNumber, medicationPayload);
        return new ResponseEntity<>(new ApiResponse("medication items were added successfully", loadedDrone),
                HttpStatus.OK);
    }

    private boolean checkDuplicates(List<MedicationPayload> medicationPayloads) {
        Set<String> medicationCodes = new HashSet<>();
        for (MedicationPayload medicationPayload : medicationPayloads) {
            if (!medicationCodes.add(medicationPayload.getMedicationCode())) {
                return false;
            }
        }
        return true;
    }

    @GetMapping("available")
    public ResponseEntity<ApiResponse> getAvailableDrones() {
        List<Drone> drones = droneService.getAvailableDrones();
        ApiResponse response = new ApiResponse(
                "available drones are loaded successfully",
                drones
        );

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("{serialNumber}/battery")
    public ResponseEntity<ApiResponse> getBattery(@PathVariable String serialNumber) {
        Double batteryLevel = droneService.getBatteryLevel(serialNumber);
        return new ResponseEntity<>(new ApiResponse(
                "battery capacity calculated successfully",
                new BatteryLevelRecord(serialNumber, batteryLevel + "%")
        ), HttpStatus.OK);
    }

    @GetMapping("{serialNumber}/medication")
    public ResponseEntity<ApiResponse> getMedication(@PathVariable String serialNumber) {
        List<MedicationItem> medicationItems = droneService.getMedicationItems(serialNumber);
        return new ResponseEntity<>(new ApiResponse(
                "medication items were found successfully",
                medicationItems
        ), HttpStatus.OK);
    }
}
