package com.artemii.musalasoft;

import com.artemii.musalasoft.model.Drone;
import com.artemii.musalasoft.model.DroneModel;
import com.artemii.musalasoft.model.DroneState;
import com.artemii.musalasoft.model.Medication;
import com.artemii.musalasoft.repository.DroneRepository;
import com.artemii.musalasoft.repository.MedicationRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class DroneManagerApplication {
    public static void main(String[] args) {
        SpringApplication.run(DroneManagerApplication.class, args);
    }

//    @Bean
//    CommandLineRunner run(MedicationRepository medicRepo, DroneRepository droneRepo) {
//        return args -> {
//            Drone drone = new Drone();
//            drone.setSerialNumber("JKHUq67hhI");
//            drone.setModel(DroneModel.Cruiserweight);
//            drone.setWeightLimit(475);
//            drone.setBatteryCapacity(80);
//            drone.setState(DroneState.IDLE);
//            droneRepo.save(drone);
//
//            Drone drone2 = new Drone();
//            drone2.setSerialNumber("JKHUq67hhIrt");
//            drone2.setModel(DroneModel.Cruiserweight);
//            drone2.setWeightLimit(300);
//            drone2.setBatteryCapacity(80);
//            drone2.setState(DroneState.IDLE);
//            droneRepo.save(drone2);
//
//
//            // Medicine
//            Medication medication = new Medication();
//            medication.setCode("N02B");
//            medication.setImage("analgin.png");
//            medication.setName("analgin");
//            medication.setWeight(200);
//
//            Medication medication2 = new Medication();
//            medication2.setCode("N02B2");
//            medication2.setImage("analgin.png");
//            medication2.setName("paracetamol");
//            medication2.setWeight(100);
//
//            Medication medication3 = new Medication();
//            medication3.setCode("N02B3");
//            medication3.setImage("analgin.png");
//            medication3.setName("paracetamol-new");
//            medication3.setWeight(600);
//
//            medicRepo.save(medication);
//            medicRepo.save(medication2);
//            medicRepo.save(medication3);
//        };
//    }
}
