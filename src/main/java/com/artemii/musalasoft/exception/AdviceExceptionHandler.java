package com.artemii.musalasoft.exception;

import com.artemii.musalasoft.model.ApiResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.validation.ConstraintViolationException;

@ControllerAdvice
@Slf4j
public class AdviceExceptionHandler {

    @ExceptionHandler(value = {RuntimeException.class})
    public ResponseEntity<ApiResponse> handleRuntimeException(RuntimeException ex) {
        log.error("exception: ", ex);
        return new ResponseEntity<>(new ApiResponse("internal server error", ex.getMessage()),
                HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(value = {DroneNotFoundException.class})
    public ResponseEntity<ApiResponse> handleDroneNotFoundException(DroneNotFoundException ex) {
        log.error("exception: ", ex);
        return new ResponseEntity<>(new ApiResponse("drone not found", ex.getMessage()),
                HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = {MedicationNotFoundException.class})
    public ResponseEntity<ApiResponse> handleMedicationNotFoundException(MedicationNotFoundException ex) {
        log.error("exception: ", ex);
        return new ResponseEntity<>(new ApiResponse("medication not found", ex.getMessage()),
                HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = {MedicationLoadingException.class})
    public ResponseEntity<ApiResponse> handleMedicationLoadingException(MedicationLoadingException ex) {
        log.error("exception: ", ex);
        return new ResponseEntity<>(new ApiResponse("error during drone loading", ex.getMessage()),
                HttpStatus.BAD_REQUEST);
    }
}
