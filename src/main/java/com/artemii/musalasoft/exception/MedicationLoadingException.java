package com.artemii.musalasoft.exception;

public class MedicationLoadingException extends RuntimeException {
    public MedicationLoadingException(String message) {
        super(message);
    }
}
