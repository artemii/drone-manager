package com.artemii.musalasoft.repository;

import com.artemii.musalasoft.model.Drone;
import com.artemii.musalasoft.model.DroneState;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface DroneRepository extends JpaRepository<Drone, Long> {

    Optional<Drone> findBySerialNumber(String seriesNumber);

    List<Drone> findByState(DroneState state);
}
