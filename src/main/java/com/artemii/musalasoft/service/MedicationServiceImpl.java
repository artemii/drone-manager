package com.artemii.musalasoft.service;

import com.artemii.musalasoft.exception.MedicationNotFoundException;
import com.artemii.musalasoft.model.Medication;
import com.artemii.musalasoft.repository.MedicationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MedicationServiceImpl implements MedicationService {

    private MedicationRepository medicationRepository;

    @Autowired
    public MedicationServiceImpl(MedicationRepository medicationRepository) {
        this.medicationRepository = medicationRepository;
    }

    @Override
    public Medication register(Medication medication) {
        return medicationRepository.save(medication);
    }

    @Override
    public Medication getByCode(String code) {
        return medicationRepository.findByCode(code)
                .orElseThrow(() ->
                        new MedicationNotFoundException("medication with code: '" + code + "' is not registered")
                );
    }
}
