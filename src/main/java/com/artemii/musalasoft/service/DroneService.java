package com.artemii.musalasoft.service;

import com.artemii.musalasoft.model.Drone;
import com.artemii.musalasoft.model.MedicationItem;
import com.artemii.musalasoft.model.MedicationPayload;

import java.util.List;

public interface DroneService {
    Drone register(Drone drone);

    Drone getBySerialNumber(String serialNumber);

    Drone addMedicationItems(String serialNumber, List<MedicationPayload> medicationPayloads);

    List<MedicationItem> getMedicationItems(String serialNumber);

    double getBatteryLevel(String serialNumber);

    List<Drone> getAvailableDrones();

}
