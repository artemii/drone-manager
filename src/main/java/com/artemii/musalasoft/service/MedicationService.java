package com.artemii.musalasoft.service;

import com.artemii.musalasoft.model.Medication;

public interface MedicationService {
    Medication register(Medication medication);

    Medication getByCode(String code);
}
