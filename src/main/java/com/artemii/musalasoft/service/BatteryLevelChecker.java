package com.artemii.musalasoft.service;

import com.artemii.musalasoft.model.BatteryLevelRecord;
import com.artemii.musalasoft.model.Drone;
import com.artemii.musalasoft.repository.DroneRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class BatteryLevelChecker {

    private DroneRepository droneRepository;

    @Autowired
    public BatteryLevelChecker(DroneRepository droneRepository) {
        this.droneRepository = droneRepository;
    }

    @Scheduled(fixedDelayString = "${drone-manager.battery.checker.time.delay:120000}", // each 2 min
            initialDelayString = "${drone-manager.battery.checker.time.init:2000}")
    public void checkBattery() {
        log.info("Start a periodic task to check drones battery levels");
        List<Drone> drones = droneRepository.findAll();
        for (Drone drone : drones) {
            log.info(new BatteryLevelRecord(drone.getSerialNumber(), drone.getBatteryCapacity() + "%").toString());
        }
    }

}
