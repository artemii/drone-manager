package com.artemii.musalasoft.service;

import com.artemii.musalasoft.exception.DroneNotFoundException;
import com.artemii.musalasoft.exception.MedicationLoadingException;
import com.artemii.musalasoft.model.*;
import com.artemii.musalasoft.repository.DroneRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
public class DroneServiceImpl implements DroneService {

    private final DroneRepository droneRepository;
    private MedicationService medicationService;

    @Autowired
    public DroneServiceImpl(DroneRepository droneRepository,
                            MedicationService medicationService) {
        this.droneRepository = droneRepository;
        this.medicationService = medicationService;
    }

    @Override
    public Drone register(Drone drone) {
        if (drone.getState() == null) {
            drone.setState(DroneState.IDLE);
        }
        return droneRepository.save(drone);
    }

    @Override
    public Drone getBySerialNumber(String serialNumber) {
        return getDrone(serialNumber);
    }

    @Override
    @Transactional
    public Drone addMedicationItems(String serialNumber, List<MedicationPayload> medicationPayloads) {
        Drone drone = getDrone(serialNumber);
        List<MedicationItem> medications = findMedications(medicationPayloads);
        double medicationWeight = sumMedicationWeight(medications);
        if (drone.getWeightLimit() >= medicationWeight) {
            setState(drone, DroneState.LOADING);
            // loading
            drone.getMedicationItems().addAll(medications);

            // finish loading process
            setState(drone, DroneState.LOADED);
            return drone;
        } else {
            throw new MedicationLoadingException("unable to add medication payload! " +
                    "Medication weight is " + medicationWeight + " but weight limit of the drone is " + drone.getWeightLimit());
        }
    }

    private void setState(Drone drone, DroneState droneState) {
        drone.setState(droneState);
        droneRepository.save(drone);
    }


    private List<MedicationItem> findMedications(List<MedicationPayload> medicationPayloads) {
        List<MedicationItem> medicationItems = new ArrayList<>();
        for (MedicationPayload medicationPayload : medicationPayloads) {
            Medication medication = medicationService.getByCode(medicationPayload.getMedicationCode());
            medicationItems.add(new MedicationItem(medication, medicationPayload.getCount()));
        }
        return medicationItems;
    }

    private double sumMedicationWeight(List<MedicationItem> medicationPayloads) {
        return medicationPayloads.stream()
                .map(medicationItem -> medicationItem.getMedication().getWeight() * medicationItem.getCount())
                .mapToDouble(Double::doubleValue).sum();
    }

    @Override
    public List<MedicationItem> getMedicationItems(String serialNumber) {
        return getDrone(serialNumber)
                .getMedicationItems();
    }

    private Drone getDrone(String serialNumber) {
        return droneRepository.findBySerialNumber(serialNumber)
                .orElseThrow(() -> new DroneNotFoundException(String.format("drone with series number: '%s' is not registered",
                        serialNumber)));
    }

    @Override
    public double getBatteryLevel(String serialNumber) {
        return getDrone(serialNumber).getBatteryCapacity();
    }

    @Override
    public List<Drone> getAvailableDrones() {
        return droneRepository.findByState(DroneState.IDLE);
    }
}
