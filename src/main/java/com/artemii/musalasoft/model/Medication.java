package com.artemii.musalasoft.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.io.Serializable;

@Data
@Entity
@Table(name = "medications")
public class Medication implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonIgnore
    private Long id;

    @Column(nullable = false)
    @NotEmpty
    @Pattern(regexp = "^[\\w-]*$", message = "allowed only letters, numbers, ‘-‘, ‘_")
    private String name;

    @Column(nullable = false)
    @NotNull
    private double weight;

    @Column(nullable = false, unique = true)
    @NotEmpty
    @Pattern(regexp = "^[A-Z0-9_]*$", message = "allowed only upper case letters, underscore and numbers")
    private String code;

    @Column(nullable = false)
    @NotEmpty
    private String image;
}
