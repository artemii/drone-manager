package com.artemii.musalasoft.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@Table(name = "drones")
public class Drone {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonIgnore
    private Long id;

    @Column(nullable = false, unique = true)
    @NotEmpty
    @Size(max = 100, message = "Serial number can't be more than 100 character")
    private String serialNumber;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private DroneModel model;

    @Column(nullable = false, scale = 2)
    @Max(value = 500, message = "drone weight limit is 500gr")
    @Min(value = 0, message = "minimum drone weight limit is 0gr")
    private double weightLimit;

    @Column(nullable = false, scale = 2)
    @Max(value = 100, message = "battery capacity can't be more than 100%")
    @Min(value = 0, message = "battery capacity can't be less than 0%")
    private double batteryCapacity;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private DroneState state;

    @OneToMany(cascade = CascadeType.ALL)
    private List<MedicationItem> medicationItems = new ArrayList<>();
}
