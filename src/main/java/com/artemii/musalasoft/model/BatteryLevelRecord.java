package com.artemii.musalasoft.model;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class BatteryLevelRecord {
    private final LocalDateTime timestamp;
    private String serialNumber;
    private String batteryCapacity;

    public BatteryLevelRecord(String serialNumber, String batteryCapacity) {
        this.serialNumber = serialNumber;
        this.batteryCapacity = batteryCapacity;
        this.timestamp = LocalDateTime.now();
    }

    @Override
    public String toString() {
        return "BatteryLevelRecord{" +
                "timestamp=" + timestamp +
                ", serialNumber of a drone ='" + serialNumber + '\'' +
                ", batteryCapacity ='" + batteryCapacity + '\'' +
                '}';
    }
}
