package com.artemii.musalasoft.model;

import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
public class MedicationPayload {
    @NotEmpty(message = "medication code must be specified")
    private String medicationCode;
    private int count;

    public int getCount() {
        return count == 0 ? 1 : count;
    }
}
