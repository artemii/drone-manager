package com.artemii.musalasoft.model;

public enum DroneModel {
    Lightweight, Middleweight, Cruiserweight, Heavyweight
}
