package com.artemii.musalasoft.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@Table(name = "medicationItems")
@NoArgsConstructor
public class MedicationItem {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonIgnore
    private long id;

    @OneToOne
    @JoinColumn(name = "medicationCode", referencedColumnName = "code")
    private Medication medication;
    private int count;

    public MedicationItem(Medication medication, int count) {
        this.medication = medication;
        this.count = count;
    }
}
