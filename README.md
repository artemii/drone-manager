## Drones

[[_TOC_]]

---

:scroll: **START**


### Introduction

There is a major new technology that is destined to be a disruptive force in the field of transportation: **the drone**. Just as the mobile phone allowed developing countries to leapfrog older technologies for personal communication, the drone has the potential to leapfrog traditional transportation infrastructure.

Useful drone functions include delivery of small items that are (urgently) needed in locations with difficult access.

---

### Task description

We have a fleet of **10 drones**. A drone is capable of carrying devices, other than cameras, and capable of delivering small loads. For our use case **the load is medications**.

A **Drone** has:
- serial number (100 characters max);
- model (Lightweight, Middleweight, Cruiserweight, Heavyweight);
- weight limit (500gr max);
- battery capacity (percentage);
- state (IDLE, LOADING, LOADED, DELIVERING, DELIVERED, RETURNING).

Each **Medication** has: 
- name (allowed only letters, numbers, ‘-‘, ‘_’);
- weight;
- code (allowed only upper case letters, underscore and numbers);
- image (picture of the medication case).

Develop a service via REST API that allows clients to communicate with the drones (i.e. **dispatch controller**). The specific communicaiton with the drone is outside the scope of this task. 

The service should allow:
- registering a drone;
- loading a drone with medication items;
- checking loaded medication items for a given drone; 
- checking available drones for loading;
- check drone battery level for a given drone;

> Feel free to make assumptions for the design approach. 

---

### Requirements

While implementing your solution **please take care of the following requirements**: 

#### Functional requirements

- There is no need for UI;
- Prevent the drone from being loaded with more weight that it can carry;
- Prevent the drone from being in LOADING state if the battery level is **below 25%**;
- Introduce a periodic task to check drones battery levels and create history/audit event log for this.

---

#### Non-functional requirements

- Input/output data must be in JSON format;
- Your project must be buildable and runnable;
- Your project must have a README file with build/run/test instructions (use DB that can be run locally, e.g. in-memory, via container);
- Required data must be preloaded in the database.
- JUnit tests are optional but advisable (if you have time);
- Advice: Show us how you work through your commit history.

---

:scroll: **END** 

---

### How to run 
Prerequisite:

1) Java 17+
2) Maven 
3) git

1) Clone the project
```shell
	git clone https://gitlab.com/artemii/drone-manager.git
```

2) Navigate to root folder
```shell
	cd drone-manager/
```

3) Run
```shell
	mvn spring-boot:run
```
Application will run on 8080 port. So this port should be available on your machine.

4) Open another terminal window and call APIs

---

### API testing

#### Assumption
- Multiple medicines can be loaded to a drone at once. Medicine weight can not exceed the weight limit of the drone
- Drone can be loaded only once. When loading process finishes, the drone will have 'LOADED' state and won't be available
- Before medication loading you have to register this medication
- ContentType is application/json 
- Base URL: http://localhost:8080/api/

---

#### Register drone:
Endpoint: POST /drone/register
Request body:   
Drone attributes:  
	- serial number (100 characters max)  
	- model (Lightweight, Middleweight, Cruiserweight, Heavyweight)  
	- weight limit (500 gr max)  
	- battery capacity (percentage)  
	- state (IDLE, LOADING, LOADED, DELIVERING, DELIVERED, RETURNING)

``` json
{
    "serialNumber": "JKHUq67hhI", 
    "model": "Cruiserweight", 
    "weightLimit": 475, 
    "batteryCapacity": 80, 
    "state": "IDLE"
} 
```

Success Response
``` json
{
	"message": "drone was registered successfully",
	"data": {
		"serialNumber": "JKHUq67hhI",
		"model": "Cruiserweight",
		"weightLimit": 475.0,
		"batteryCapacity": 80.0,
		"state": "IDLE",
		"medicationItems": []
	}
}
```

Error response 
``` json
{
  "message" : "validation error",
  "data" : {
    "weightLimit" : "drone weight limit is 500gr"
  }
}
```

Request sample

```shell
curl -s -X POST http://localhost:8080/api/drone/register -H 'Content-Type: application/json' -d '
{
    "serialNumber": "JKHUq67hhI", 
    "model": "Cruiserweight", 
    "weightLimit": 475, 
    "batteryCapacity": 80, 
    "state": "IDLE"
}
'
```

---

#### Get available drones
Endpoint: GET drone/available


Success Response
``` json
{
  "message" : "available drones are loaded successfully",
  "data" : [ {
    "serialNumber" : "JKHUq67hhIrt",
    "model" : "Cruiserweight",
    "weightLimit" : 300.0,
    "batteryCapacity" : 80.0,
    "state" : "IDLE",
    "medicationItems" : [ ]
  } ]
}
```

Error response 
``` json
{
  "message" : "drone not found",
  "data" : "drone with series number: 'JKHUq67hI' is not registered"
}
```

Request sample

```shell
curl -s -X GET http://localhost:8080/api/drone/available -H 'Content-Type: application/json'
```

---

#### Register medicine:
Endpoint: POST /medication/register  
Request body:   
Medication attributes:  
	- name(allowed only letters, numbers, ‘-‘, ‘_’)  
	- weight  
	- code (allowed only upper case letters, underscore and numbers)  
	- image (picture of the medication case)  

``` json
{
    "name": "analgin", 
    "code": "N02B", 
    "weight": 200, 
    "image": "analgin.png"
} 

```

Success Response
``` json
{
  "message" : "medication was registered successfully",
  "data" : {
    "name" : "analgin",
    "weight" : 200.0,
    "code" : "N02B",
    "image" : "analgin.png"
  }
}
```

Error response 
``` json
{
  "message" : "input data is not valid!",
  "data" : {
    "name" : "allowed only letters, numbers, ‘-‘, ‘_"
  }
}
```

Request sample

```shell
curl -s -X POST http://localhost:8080/api/medication/register -H 'Content-Type: application/json' -d '
{
    "name": "analgin", 
    "code": "N02B", 
    "weight": 200, 
    "image": "analgin.png"
} 
'
```

---

#### Load medicine:
Endpoint: POST drone/{serialNumber}/medication/load  
Request body:   
	- If count is not specified then it's considered as 1  
	- Drone battery level must be more than 25%  
	- Drone must be in IDLE state  
	- medication weight must be less then drone weight limit  
	
``` json
[{
	"medicationCode": "N02B",
	"count": 2
}]
```

Success Response
``` json
{
  "message" : "medication items were added successfully",
  "data" : {
    "serialNumber" : "JKHUq67hhI",
    "model" : "Cruiserweight",
    "weightLimit" : 475.0,
    "batteryCapacity" : 80.0,
    "state" : "LOADED",
    "medicationItems" : [ {
      "medication" : {
        "name" : "analgin",
        "weight" : 200.0,
        "code" : "N02B",
        "image" : "analgin.png"
      },
      "count" : 2
    } ]
  }
}

```

Error response 
``` json
{
  "message" : "the drone is not available. Drone state: LOADED",
  "data" : {
    "serialNumber" : "JKHUq67hhI",
    "model" : "Cruiserweight",
    "weightLimit" : 475.0,
    "batteryCapacity" : 80.0,
    "state" : "LOADED",
    "medicationItems" : [ {
      "medication" : {
        "name" : "analgin",
        "weight" : 200.0,
        "code" : "N02B",
        "image" : "analgin.png"
      },
      "count" : 2
    } ]
  }
}

```

Request sample

```shell
curl -s -X POST http://localhost:8080/api/drone/JKHUq67hhI/medication/load -H 'Content-Type: application/json' -d '
[{
	"medicationCode": "N02B",
	"count": 2
}] 
'
```

---

#### Get loaded medication items 
Endpoint: GET drone/{serialNumber}/medication


Success Response
``` json
{
  "message" : "medication items were found successfully",
  "data" : [ {
    "medication" : {
      "name" : "analgin",
      "weight" : 200.0,
      "code" : "N02B",
      "image" : "analgin.png"
    },
    "count" : 2
  } ]
}


```

Error response 
``` json
{
  "message" : "drone not found",
  "data" : "drone with series number: 'JKHUq67h' is not registered"
}
```

Request sample

```shell
curl -s -X GET http://localhost:8080/api/drone/JKHUq67hhI/medication -H 'Content-Type: application/json'
```

---

#### Get drone's battery capacity
Endpoint: GET drone/{serialNumber}/battery


Success Response
``` json
{
  "message" : "battery capacity calculated successfully",
  "data" : {
    "timestamp" : "2022-06-13T18:07:30.452708",
    "serialNumber" : "JKHUq67hhI",
    "batteryCapacity" : "80.0%"
  }
}
```

Error response 
``` json
{
  "message" : "drone not found",
  "data" : "drone with series number: 'JKHUq67hI' is not registered"
}
```

Request sample

```shell
curl -s -X GET http://localhost:8080/api/drone/JKHUq67hhI/battery -H 'Content-Type: application/json'
```